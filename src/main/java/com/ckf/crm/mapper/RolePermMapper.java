package com.ckf.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ckf.crm.entity.RolePerm;
import org.springframework.stereotype.Repository;

/**
 * @author 安详的苦丁茶
 * @version 1.0
 * @date 2020/3/28 14:54
 */

@Repository
public interface RolePermMapper extends BaseMapper<RolePerm> {
}
